import os.path
import time
import re
import torch

try:
    from tqdm import tqdm
except:
    pass

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
from torch.utils.tensorboard import SummaryWriter


try:
    from ding.logger import Logger
except ImportError:
    class Logger:

        def __init__(self, run_name=None, root='runs', tensorboard=True, use_tqdm=True, **kwargs):
            if run_name is None:
                run_name = re.sub(r'\s+|:', '_', time.asctime())

            self.run_name = run_name
            self.use_tensorboard = tensorboard

            self.scalars = {}
            self.epoch = 0

            self.directory = os.path.join(root, run_name)
            if self.use_tensorboard:
                if not os.path.exists(root):
                    os.mkdir(root)
                if not os.path.exists(self.directory):
                    os.mkdir(self.directory)

            self.use_tqdm = use_tqdm

            self.writer = SummaryWriter(log_dir=self.directory, flush_secs=5) if tensorboard else None

        def epoch_range(self, *r):
            r = range(*r)
            for i in r:
                print("Epoch {} / {}".format(i, r.stop - r.step))
                self.epoch = i
                yield i

            self._end_training()

        def enable_checkpoint(self, *args, **kwargs):
            warnings.warn("Checkpoints not implemented")

        def resume(self):
            warnings.warn("Checkpoints not implemented")

        def add_scalar(self, tag: str, value, step: int = None, **kwargs):
            if isinstance(value, torch.Tensor):
                value = value.item()

            if tag not in self.scalars:
                self.scalars[tag] = {
                    'value': value,
                    'step': 0 if step is None else step,
                }
            else:
                self.scalars[tag]['value'] = value
                self.scalars[tag]['step'] = self.scalars[tag]['step'] + 1 if step is None else step

            if self.writer is not None:
                self.writer.add_scalar(tag, value, self.scalars[tag]['step'])

        def print_scalars(self, tags=None):
            if tags is None:
                tags = self.scalars.keys()

            for t in tags:
                print("{} = {:.5f} %".format(t, self.scalars[t]['value']))

        def progress(self, total, action):
            if self.use_tqdm:
                return tqdm(total=total, unit='samples', desc=action)
            else:
                # Alternative output if tqdm is not enabled
                class ProgressIndicator:
                    def __init__(self, logger: Logger):
                        self.total = total
                        self.action = action
                        self.counter = 0
                        self.logger = logger
                        self.old_line = ''

                    def __enter__(self):
                        self.logger.current_action = self.action
                        return self

                    def __exit__(self, exc_type, exc_val, exc_tb):
                        self.logger.current_action = "Idle"

                    def update(self, n: int):
                        self.counter += n
                        line = "{} (epoch {}) ({} %)".format(self.logger.current_action, self.logger.epoch,
                                                             int(self.counter / self.total * 100))
                        if line != self.old_line:
                            print(line)
                            self.old_line = line

                return ProgressIndicator(logger=self)

        @staticmethod
        def _end_training():
            print("End")
