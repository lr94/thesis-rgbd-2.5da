#!/usr/bin/env python3
"""
    Import packages
"""

import numpy as np
import argparse
import os

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

from net import ResBase, ResClassifierMMD
from data_loader import DatasetGeneratorMultimodal, MyTransformer
from utils import OptimizerManager, EvaluationManager, IteratorWrapper, weights_init, default_paths, map_to_device,\
    add_base_args
from logger import Logger

"""
    Parse arguments
"""

parser = argparse.ArgumentParser()

add_base_args(parser)
parser.add_argument("--extract", default=True)
parser.add_argument("--task", default="rgbd-mmd")
parser.add_argument('--no_tqdm', action='store_true')

# hyper params
parser.add_argument("--weight_mmd", default=1.0, type=float)

args = parser.parse_args()

noise_suffix = args.depth_noise
if noise_suffix is not None:
    noise_suffix = {
        'n': '_noisy',
        'ln': '_less_noisy'
    }[noise_suffix]

# Load default paths if needed
default_paths(args)

# Tensorboard summary
hp_list = [args.task, args.net, args.epoch, args.lr, args.lr_mult, args.batch_size, args.weight_mmd]
hp_list = [str(hp) for hp in hp_list]
hp_string = '_'.join(hp_list) + args.suffix
print("Run: " + hp_string)
writer = Logger(run_name=hp_string, root=args.tensorboard, use_tqdm=not args.no_tqdm)

# Device
device = torch.device('cuda:{}'.format(args.gpu))

"""
    Define input pipeline
"""

# Source

test_transform = MyTransformer([int( (256-224)/2 ), int( (256-224)/2 )], False)

train_set_source = DatasetGeneratorMultimodal(args.data_root_source, args.train_file_source, do_rot=False,
                                              ds_name=args.source, d_suffix=noise_suffix)
test_set_source = DatasetGeneratorMultimodal(args.data_root_source, args.test_file_source, do_rot=False,
                                             transform=test_transform, ds_name=args.source, d_suffix=noise_suffix)

# Source training recognition (1)
train_loader_source = torch.utils.data.DataLoader(train_set_source,
                                                  shuffle=True,
                                                  batch_size=args.batch_size,
                                                  num_workers=args.num_workers)

# Source test recognition (1)
test_loader_source = torch.utils.data.DataLoader(test_set_source,
                                                 shuffle=True,
                                                 batch_size=args.batch_size,
                                                 num_workers=args.num_workers)

train_set_target = DatasetGeneratorMultimodal(args.data_root_target, args.train_file_target, ds_name=args.target,
                                              do_rot=False)

# Target train
train_loader_target = torch.utils.data.DataLoader(train_set_target,
                                                  shuffle=True,
                                                  batch_size=args.batch_size,
                                                  num_workers=args.num_workers)

# Target test (1)
test_loader_target = torch.utils.data.DataLoader(train_set_target,
                                                 shuffle=True,
                                                 batch_size=args.batch_size,
                                                 num_workers=args.num_workers)

"""
    Set up network & optimizer
"""
input_dim_F = 2048 if args.net == 'resnet50' else 512
netG_rgb = ResBase(architecture=args.net)
netG_depth = ResBase(architecture=args.net)
netF = ResClassifierMMD(input_dim=input_dim_F * 2, class_num=args.class_num, dropout_p=args.dropout_p)
netF.apply(weights_init)

net_list = [netG_rgb, netG_depth, netF]
net_list = map_to_device(device, net_list)

ce_loss = nn.CrossEntropyLoss()

opt_g_rgb = optim.SGD(netG_rgb.parameters(), lr=args.lr, momentum=0.9, weight_decay=args.weight_decay)
opt_g_depth = optim.SGD(netG_depth.parameters(), lr=args.lr, momentum=0.9, weight_decay=args.weight_decay)
opt_f = optim.SGD(netF.parameters(), lr=args.lr * args.lr_mult, momentum=0.9, weight_decay=args.weight_decay)

optims_list = [opt_g_rgb, opt_g_depth, opt_f]

for epoch in writer.epoch_range(1, args.epoch + 1):
    # ========================= TRAINING =========================

    # Train source (for recognition)
    train_loader_source_rec_iter = train_loader_source
    # Train target
    train_target_loader_iter = IteratorWrapper(train_loader_target)
    # Test target
    test_target_loader_iter = IteratorWrapper(test_loader_target)

    with writer.progress(total=len(train_loader_source), action="Train") as pb:
        for batch_num, (img_rgb, img_depth, img_label_source) in enumerate(train_loader_source_rec_iter):
            # if img_rgb.size(0) != args.batch_size:
            # TODO remove
            #if batch_num > 50:
            #    break
            if img_rgb.size(0) != args.batch_size:
                break

            with OptimizerManager(optims_list):
                loss = 0.0

                # Compute source features
                img_rgb, img_depth, img_label_source = map_to_device(device, (img_rgb, img_depth, img_label_source))
                feat_rgb, _ = netG_rgb(img_rgb)
                feat_depth, _ = netG_depth(img_depth)
                features_source = torch.cat((feat_rgb, feat_depth), 1)

                # Compute target features
                img_rgb, img_depth, _ = train_target_loader_iter.get_next()
                img_rgb, img_depth = map_to_device(device, (img_rgb, img_depth))
                feat_rgb, _ = netG_rgb(img_rgb)
                feat_depth, _ = netG_depth(img_depth)
                features_target = torch.cat((feat_rgb, feat_depth), 1)

                logits, loss_mmd_train = netF(features_source, features_target)

                loss_rec = ce_loss(logits, img_label_source)
                loss = loss_rec + loss_mmd_train * args.weight_mmd

                del img_rgb, img_depth, img_label_source, feat_rgb, feat_depth, logits

                # Compute gradient and update
                loss.backward()
                pb.update(1)

    # ========================= VALIDATION =========================

    # Recognition - source
    with EvaluationManager(net_list), writer.progress(total=args.test_batches, action="TestRecS") as pb:
        test_source_loader_iter = iter(test_loader_source)
        correct = 0.0
        num_predictions = 0.0
        val_loss = 0.0
        val_loss_mmd_tot = 0.0
        for num_batch, (img_rgb, img_depth, img_label_source) in enumerate(test_source_loader_iter):
            # Validate only on 100 batches
            if num_batch >= args.test_batches:
                break
            pb.update(1)

            # Compute source features
            img_rgb, img_depth, img_label_source = map_to_device(device, (img_rgb, img_depth, img_label_source))
            feat_rgb, _ = netG_rgb(img_rgb)
            feat_depth, _ = netG_depth(img_depth)
            features_source = torch.cat((feat_rgb, feat_depth), 1)

            # Compute target features
            img_rgb, img_depth, _ = test_target_loader_iter.get_next()
            img_rgb, img_depth = map_to_device(device, (img_rgb, img_depth))
            feat_rgb, _ = netG_rgb(img_rgb)
            feat_depth, _ = netG_depth(img_depth)
            features_target = torch.cat((feat_rgb, feat_depth), 1)

            preds, val_loss_mmd_batch = netF(features_source, features_target)
            val_loss_mmd_tot += val_loss_mmd_batch.item()

            val_loss += ce_loss(preds, img_label_source).item()
            gt = img_label_source
            correct += (torch.argmax(preds, dim=1) == gt).sum().item()
            num_predictions += preds.shape[0]

        val_acc = 1.0 * correct / num_predictions
        val_loss = val_loss / args.test_batches
        val_loss_mmd_tot = val_loss_mmd_tot / args.test_batches
        print("Epoch: {} - Validation source accuracy (recognition): {}".format(epoch, val_acc))

    del img_rgb, img_depth, img_label_source, feat_rgb, feat_depth, preds, gt

    writer.add_scalar("Loss/train", loss_rec.item(), epoch)
    writer.add_scalar("Loss/mmd", loss_mmd_train.item(), epoch)
    writer.add_scalar("Loss/mmd_val", val_loss_mmd_tot)
    writer.add_scalar("Loss/val", val_loss, epoch)
    writer.add_scalar("Accuracy/val", val_acc, epoch)

    # Save models
    if epoch % 5 == 0:
        if not os.path.exists(args.snapshot):
            os.mkdir(args.snapshot)

        torch.save(netG_rgb.state_dict(),
                   os.path.join(args.snapshot, hp_string + "_netG_rgb_epoch" + str(epoch) + ".pth"))
        torch.save(netG_depth.state_dict(),
                   os.path.join(args.snapshot, hp_string + "_netG_depth_epoch" + str(epoch) + ".pth"))
        torch.save(netF.state_dict(), os.path.join(args.snapshot, hp_string + "_netF_rgbd_epoch" + str(epoch) + ".pth"))

