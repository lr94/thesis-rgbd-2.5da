import torch
import torch.utils.data as data

from PIL import Image
import os
import random
import numpy as np
import torchvision.transforms.functional as TF

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


def default_loader(path):
    return Image.open(path).convert('RGB')


def make_dataset(root, label):
    images = []
    labeltxt = open(label)
    for line in labeltxt:
        data = line.strip().split(' ')
        if is_image_file(data[0]):
            path = os.path.join(root, data[0])
        gt = int(data[1])
        item = (path, gt)
        images.append(item)
    return images


def make_sync_dataset(root, label, ds_name='synARID', rgb_suffix=None, d_suffix=None):
    rgb_suffix = rgb_suffix or ''
    d_suffix = d_suffix or ''

    images = []
    labeltxt = open(label)
    for line in labeltxt:
        data = line.strip().split(' ')
        if is_image_file(data[0]):
            path = os.path.join(root, data[0])
        if ds_name == 'synARID' or ds_name in ['synHB', 'valHB']:
            path_rgb = path.replace('***', 'rgb' + rgb_suffix)
            path_depth = path.replace('***', 'depth' + d_suffix)
        elif ds_name == 'ARID' or ds_name == 'ARID-RL75I':
            path_rgb = path.replace('***', 'crop')
            path_rgb = path_rgb.replace('???', 'rgb_padding' + rgb_suffix)
            path_depth = path.replace('***', 'depthcrop')
            path_depth = path_depth.replace('???', 'arid_depth_distance' + d_suffix)
        elif ds_name == 'wrgbd':
            path_rgb = path.replace('***', 'crop')
            path_rgb = path_rgb.replace('???', 'rgb' + rgb_suffix)
            path_depth = path.replace('***', 'depthcrop')
            path_depth = path_depth.replace('???', 'surfnorm' + d_suffix)
        else:
            raise ValueError('DATASET NAME IS INCORRECT: insert synARID, ARID, ARID-RL75I, synHB, valHB or wrgbd')
        gt = int(data[1])
        item = (path_rgb, path_depth, gt)
        images.append(item)
    return images


def get_relative_rotation(rgb_rot, depth_rot):
    rel_rot = rgb_rot - depth_rot
    if rel_rot < 0:
        rel_rot += 4
    assert rel_rot in range(4)
    return rel_rot


class MyTransformer(object):
    """docstring for ClassName"""

    def __init__(self, crop, flip):
        super(MyTransformer, self).__init__()
        self.crop = crop
        self.flip = flip
        self.angles = [0, 90, 180, 270]

    def __call__(self, img, rot=None):
        img = TF.resize(img, (256, 256))
        img = TF.crop(img, self.crop[0], self.crop[1], 224, 224)
        if self.flip:
            img = TF.hflip(img)
        if rot is not None:
            img = TF.rotate(img, self.angles[rot])
        img = TF.to_tensor(img)
        img = TF.normalize(img, [0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        return img


class DatasetGenerator(data.Dataset):
    def __init__(self, root, label, transform=None, loader=default_loader):
        imgs = make_dataset(root, label)
        self.root = root
        self.label = label
        self.imgs = imgs
        self.transform = transform
        self.loader = loader

    def __getitem__(self, index):
        path, target = self.imgs[index]
        img = self.loader(path)

        if self.transform is not None:
            img = self.transform(img)

        return img, target

    def __len__(self):
        return len(self.imgs)


class DatasetGeneratorSingleSync(data.Dataset):
    """
    This is a single modality dataset generator. It's similar to DatasetGenerator, but it implements the same data
    augmentation scheme of DatasetGeneratorMultimodal and accepts the same type of split files (sync)
    """

    def __init__(self, root, label, ds_name='synARID', modality='rgb', do_rot=False, transform=None,
                 loader=default_loader, suffix=''):
        assert modality in ['rgb', 'd']
        suffix_kwarg = {'rgb_suffix': suffix} if modality == 'rgb' else {'d_suffix': suffix}

        self.imgs = make_sync_dataset(root, label, ds_name=ds_name, **suffix_kwarg)
        self.root = root
        self.label = label
        self.transform = transform
        self.loader = loader
        self.modality = modality
        self.do_rot = do_rot

    def __getitem__(self, index):
        path_rgb, path_depth, target = self.imgs[index]
        path = path_rgb if self.modality == 'rgb' else path_depth

        img = self.loader(path)

        if self.transform is not None:
            img = self.transform(img)
        else:
            top = random.randint(0, 256 - 224)
            left = random.randint(0, 256 - 224)
            flip = random.choice([True, False])

            if self.do_rot:
                rot = random.choice([0, 1, 2, 3])
            else:
                rot = None

            transform = MyTransformer([top, left], flip)
            img = transform(img, rot)

        if self.do_rot and self.transform is None:
            return img, target, rot

        return img, target

    def __len__(self):
        return len(self.imgs)


class DatasetGeneratorMultimodal(data.Dataset):
    def __init__(self, root, label, ds_name='synARID', do_rot=False, transform=None, loader=default_loader,
                 rgb_suffix='', d_suffix=''):
        imgs = make_sync_dataset(root, label, ds_name=ds_name, rgb_suffix=rgb_suffix, d_suffix=d_suffix)
        self.root = root
        self.label = label
        self.imgs = imgs
        self.transform = transform
        self.loader = loader
        self.do_rot = do_rot

    def __getitem__(self, index):
        path_rgb, path_depth, target = self.imgs[index]
        img_rgb = self.loader(path_rgb)
        img_depth = self.loader(path_depth)

        if self.transform is not None:
            img_rgb = self.transform(img_rgb)
            img_depth = self.transform(img_depth)
        else:
            top = random.randint(0, 256 - 224)
            left = random.randint(0, 256 - 224)
            flip = random.choice([True, False])
            if self.do_rot:
                rot_rgb = random.choice([0, 1, 2, 3])
                rot_depth = random.choice([0, 1, 2, 3])
            else:
                rot_rgb = None
                rot_depth = None
            transform = MyTransformer([top, left], flip)
            img_rgb = transform(img_rgb, rot_rgb)
            img_depth = transform(img_depth, rot_depth)

        if self.do_rot and (self.transform is None):
            return img_rgb, img_depth, target, get_relative_rotation(rot_rgb, rot_depth)
        return img_rgb, img_depth, target

    def __len__(self):
        return len(self.imgs)


class DatasetGeneratorDouble(data.Dataset):
    def __init__(self, root, label, do_rot=False, transform=None, loader=default_loader):
        imgs = make_sync_dataset(root, label)
        self.root = root
        self.label = label
        self.imgs = imgs
        self.transform = transform
        self.loader = loader
        self.do_rot = do_rot

    def __getitem__(self, index):
        path_rgb, _, target = self.imgs[index]
        img_rgb = self.loader(path_rgb)

        if self.transform is not None:
            img_rgb = self.transform(img_rgb)
        else:
            top = random.randint(0, 256 - 224)
            left = random.randint(0, 256 - 224)
            flip = random.choice([True, False])
            if self.do_rot:
                rot_rgb = random.choice([0, 1, 2, 3])
                rot_depth = random.choice([0, 1, 2, 3])
            else:
                rot_rgb = None
                rot_depth = None
            transform = MyTransformer([top, left], flip)
            img_rgb_1 = transform(img_rgb, rot_rgb)
            img_rgb_2 = transform(img_rgb, rot_depth)

        if self.do_rot and (self.transform is None):
            return img_rgb_1, img_rgb_2, target, get_relative_rotation(rot_rgb, rot_depth)
        return img_rgb_1, img_rgb_2, target

    def __len__(self):
        return len(self.imgs)
