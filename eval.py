#!/usr/bin/env python3

"""
Import packages
"""

import numpy as np
import argparse
import os

import torch
import torch.nn.functional as F
from torch import nn
from torch.autograd import Variable
from torch.utils.tensorboard import SummaryWriter

from net import ResBase, ResClassifier
from data_loader import DatasetGeneratorMultimodal, MyTransformer
from utils import make_paths, add_base_args, default_paths

from logger import Logger


"""
Parse arguments
"""

data_root_source, data_root_target, split_source_train, split_source_test, split_target = make_paths()

parser = argparse.ArgumentParser()

add_base_args(parser)
parser.add_argument("--weight_da", default=1.0, type=float)
parser.add_argument('--da_method', default='noda')
parser.add_argument("--task", default=None)
parser.add_argument('--no_tqdm', action='store_true')
parser.add_argument('--output_path', default='eval_result')

args = parser.parse_args()
default_paths(args)
args.data_root = args.data_root_target
args.test_file = args.test_file_target

device = torch.device('cuda:{}'.format(args.gpu))


test_transform = MyTransformer([int( (256-224)/2 ), int( (256-224)/2 )], False)
test_set = DatasetGeneratorMultimodal(args.data_root, args.test_file, ds_name=args.target, do_rot=False)
test_loader = torch.utils.data.DataLoader(test_set,
                                        shuffle=True,
                                        batch_size=args.batch_size,
                                        num_workers=args.num_workers)

"""
Set up network & optimizer
"""
if args.net == "resnet50":
    input_dim_F = 2048
else:
    input_dim_F = 512
netG_rgb = ResBase(architecture=args.net).to(device)
netG_depth = ResBase(architecture=args.net).to(device)
netF = ResClassifier(input_dim=input_dim_F*2, class_num=args.class_num, extract=False,
					 dropout_p=args.dropout_p).to(device)
netG_rgb.eval()
netG_depth.eval()
netF.eval()

# Load network weights
hp_list = [args.task, args.net, args.epoch, args.lr, args.lr_mult, args.batch_size]
if args.da_method != 'noda':
    hp_list.append(args.weight_da)
# hp_list = [args.task, args.net, args.epoch, args.lr, args.batch_size, args.weight_decay, args.dropout_p]
hp_list = [str(hp) for hp in hp_list]
hp_string = '_'.join(hp_list) + args.suffix

logger = Logger(run_name="val_" + hp_string, root='tensorboard', use_tqdm=not args.no_tqdm, status_file=True,
                tensorboard=False)

print("Run: {}".format(hp_string))

if not os.path.exists(args.output_path):
    os.mkdir(args.output_path)

ce_loss = nn.CrossEntropyLoss()

with open(os.path.join(args.output_path, hp_string + '.csv'), 'w') as fp:
    for epoch in range(1, args.epoch + 1):
        if epoch % 5 != 0:
            continue

        cm = np.zeros([args.class_num, args.class_num], dtype=np.float32)

        netG_rgb.load_state_dict(
            torch.load(os.path.join(args.snapshot, hp_string + "_netG_rgb_epoch" + str(epoch) + ".pth"),
                       map_location=device))
        netG_depth.load_state_dict(
            torch.load(os.path.join(args.snapshot, hp_string + "_netG_depth_epoch" + str(epoch) + ".pth"),
                       map_location=device))
        netF.load_state_dict(
            torch.load(os.path.join(args.snapshot, hp_string + "_netF_rgbd_epoch" + str(epoch) + ".pth"),
                       map_location=device))

        correct = 0
        with logger.progress(total=len(test_loader), action="TestEpoch{}".format(epoch)) as pb:
            val_loss = 0.0
            n_batches = 0

            for (imgs_rgb, imgs_depth, labels) in test_loader:
                pb.update(1)
                imgs_rgb = Variable(imgs_rgb.to(device))
                imgs_depth = Variable(imgs_depth.to(device))
                feat_rgb, _ = netG_rgb(imgs_rgb)
                feat_depth, _ = netG_depth(imgs_depth)
                pred = netF(torch.cat((feat_rgb, feat_depth), 1))
                val_loss += ce_loss(pred, labels.to(device)).item()

                pred = F.softmax(pred)
                pred = pred.data.cpu().numpy()
                pred = pred.argmax(axis=1)
                labels = labels.numpy()
                correct += np.equal(labels, pred).sum().item()
                for ii in range(pred.shape[0]):
                    cm[labels[ii], pred[ii]] += 1

                n_batches += 1

        accuracy = 1.0 * np.trace(cm) / np.sum(cm).item()
        val_loss /= n_batches
        # precision, recall = compute_precision_recall(cm)
        print("{} @{} epochs: overall accuracy = {}".format(hp_string, epoch, accuracy))
        print("{} @{} epochs: overall loss = {}".format(hp_string, epoch, val_loss))
        fp.write("{},{},{},{}\n".format(hp_string, epoch, accuracy, val_loss))
        fp.flush()
        # print("Precision: {}".format(precision))
        # print("Recall: {}".format(recall))
