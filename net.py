import torch
from torch.autograd import Function
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
import math

from mmd_loss import MMDLoss


class GradientReversalLayer(Function):

    @staticmethod
    def forward(self, x, lambda_val):
        self.lambda_val = lambda_val

        return x.view_as(x)

    @staticmethod
    def backward(self, grad_out):
        grad_in = grad_out.neg() * self.lambda_val

        return grad_in, None


class DilatedProjectionBlock(nn.Module):
    def __init__(self, input_dim, projection_dim=512):
        super(DilatedProjectionBlock, self).__init__()
        self.input_dim = input_dim
        self.projection_dim = projection_dim

        self.conv_dilated = nn.Sequential(
            nn.Conv2d(self.input_dim, self.input_dim, [3, 3], stride=[3, 3], dilation=[2, 2],
                      groups=int(1.0 * self.input_dim / 64)),
            nn.BatchNorm2d(self.input_dim),
            nn.ReLU(inplace=True)
        )
        self.conv_projection = nn.Sequential(
            nn.Conv2d(self.input_dim, self.projection_dim, [3, 3], stride=[1, 1]),
            nn.BatchNorm2d(self.projection_dim),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv_dilated(x)
        x = self.conv_projection(x)
        x_dim = x.shape[2]
        x = F.max_pool2d(x, kernel_size=[x_dim, x_dim], stride=None,
                         padding=0, dilation=1, return_indices=False, ceil_mode=False)
        return x


class ResBase(nn.Module):
    def __init__(self, architecture, use_projection=False):
        super(ResBase, self).__init__()
        if architecture == 'resnet50':
            model_resnet = models.resnet50(pretrained=True)
        elif architecture == 'resnet18':
            model_resnet = models.resnet18(pretrained=True)
        else:
            print("ValueError: {} is not a valid ResNet architecture.".format(architecture))

        self.use_projection = use_projection

        self.conv1 = model_resnet.conv1
        self.bn1 = model_resnet.bn1
        self.relu = model_resnet.relu
        self.maxpool = model_resnet.maxpool
        self.layer1 = model_resnet.layer1
        self.layer2 = model_resnet.layer2
        self.layer3 = model_resnet.layer3
        self.layer4 = model_resnet.layer4
        self.avgpool = model_resnet.avgpool

        self.pb = DilatedProjectionBlock(input_dim=256)


    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        #x_p = F.max_pool2d(x, kernel_size=[7, 7], stride=[7, 7],
        #                padding=0, dilation=1, return_indices=False, ceil_mode=False)
        x = self.layer4(x)
        x_p = F.max_pool2d(x, kernel_size=[7, 7], stride=[7, 7],
                        padding=0, dilation=1, return_indices=False, ceil_mode=False)
        #x_p = x
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)

        return x, torch.flatten(x_p, start_dim=1)
        #return x, x_p



class ResClassifier(nn.Module):
    def __init__(self, input_dim=2048, class_num=51, extract=True, dropout_p=0.5):
        super(ResClassifier, self).__init__()
        self.fc1 = nn.Sequential(
            nn.Linear(input_dim, 1000),
            nn.BatchNorm1d(1000, affine=True),
            nn.ReLU(inplace=True),
            nn.Dropout(p=dropout_p)
        )
        self.fc2 = nn.Linear(1000, class_num)
        self.extract = extract
        self.dropout_p = dropout_p

    def forward(self, x):
        emb = self.fc1(x)
        logit = self.fc2(emb)

        if self.extract:
            return emb, logit
        return logit


class ConvClassifier(nn.Module):
    def __init__(self, input_dim, projection_dim=100, class_num=4):
        super(ConvClassifier, self).__init__()
        self.input_dim = input_dim
        self.projection_dim = projection_dim

        self.conv_1x1 = nn.Sequential(
            nn.Conv2d(self.input_dim, self.projection_dim, [1, 1], stride=[1, 1]),
            nn.BatchNorm2d(self.projection_dim),
            nn.ReLU(inplace=True)
        )
        self.fc = nn.Linear(projection_dim * 7 * 7, class_num)

    def forward(self, x):
        x = self.conv_1x1(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x


class ConvClassifierBig(nn.Module):
    def __init__(self, input_dim, projection_dim=100, class_num=4):
        super(ConvClassifierBig, self).__init__()
        self.input_dim = input_dim
        self.projection_dim = projection_dim

        self.conv_1x1 = nn.Sequential(
            nn.Conv2d(self.input_dim, self.projection_dim, [1, 1], stride=[1, 1]),
            nn.BatchNorm2d(self.projection_dim),
            nn.ReLU(inplace=True)
        )
        self.conv_3x3 = nn.Sequential(
            nn.Conv2d(self.projection_dim, self.projection_dim, [3, 3], stride=[2, 2]),
            nn.BatchNorm2d(self.projection_dim),
            nn.ReLU(inplace=True)
        )
        self.fc = nn.Linear(projection_dim * 3 * 3, class_num)

    def forward(self, x):
        x = self.conv_1x1(x)
        x = self.conv_3x3(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x


class ResClassifierMMD(ResClassifier):
    def __init__(self, mmd_loss=MMDLoss(), **kwargs):
        kwargs['extract'] = False
        super().__init__(**kwargs)

        self.mmd = mmd_loss

    def forward(self, source, target=None):
        if not self.training and target is None:
            return super().forward(source)

        assert target is not None

        mmd_loss = 0

        for layer in self.fc1:
            source = layer(source)
            target = layer(target)

            if isinstance(layer, nn.Linear):
                mmd_loss += self.mmd(source, target)

        source = self.fc2(source)
        target = self.fc2(target)
        mmd_loss += self.mmd(source, target)

        return source, mmd_loss
