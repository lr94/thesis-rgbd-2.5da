import argparse

import re
import functools
import platform
import warnings

import torch
import torch.nn as nn
import torch.nn.functional as F
import os.path
import numpy as np


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv2d') != -1:
        torch.nn.init.xavier_uniform_(m.weight)
        torch.nn.init.zeros_(m.bias)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.01)
        m.bias.data.fill_(0)
    elif classname.find('Linear') != -1:
        m.weight.data.normal_(0.0, 0.01)
        m.bias.data.normal_(0.0, 0.01)


def l2norm_loss_self_driven(x):
    radius = x.norm(p=2, dim=1).detach()
    norm = radius.mean()
    assert radius.requires_grad == False
    radius = radius + 1.0
    l = ((x.norm(p=2, dim=1) - radius) ** 2).mean()
    return l, norm


def entropy_loss(logits):
    p_softmax = F.softmax(logits)
    mask = p_softmax.ge(0.000001) # greater or equal to
    mask_out = torch.masked_select(p_softmax, mask)
    entropy = -(torch.sum(mask_out * torch.log(mask_out)))
    return entropy / float(p_softmax.size(0))


class OptimizerManager:
    def __init__(self, optims):
        self.optims = optims  # if isinstance(optims, Iterable) else [optims]

    def __enter__(self):
        for op in self.optims:
            op.zero_grad()

    def __exit__(self, exceptionType, exception, exceptionTraceback):
        for op in self.optims:
            op.step()
        self.optims = None
        if exceptionTraceback:
            print(exceptionTraceback)
            return False
        return True


class EvaluationManager:
    def __init__(self, nets):
        self.nets = nets

    def __enter__(self):
        self.prev = torch.is_grad_enabled()
        torch._C.set_grad_enabled(False)
        for net in self.nets:
            net.eval()

    def __exit__(self, *args):
        torch.set_grad_enabled(self.prev)
        for net in self.nets:
            net.train()
        return False

    def __call__(self, func):
        @functools.wraps(func)
        def decorate_no_grad(*args, **kwargs):
            with self:
                return func(*args, **kwargs)

        return decorate_no_grad


class IteratorWrapper:
    def __init__(self, loader):
        self.loader = loader
        self.iterator = iter(loader)

    def __iter__(self):
        self.iterator = iter(self.loader)

    def get_next(self):
        try:
            items = self.iterator.next()
        except:
            self.__iter__()
            items = self.iterator.next()
        return items


def compute_precision_recall(confusion_matrix):
    pr_denom = np.sum(confusion_matrix, axis=1)
    rec_denom = np.sum(confusion_matrix, axis=0)
    pr = []
    rec = []
    for cc in range(confusion_matrix.shape[0]):
        pr.append(1.0 * confusion_matrix[cc, cc] / pr_denom[cc])
        rec.append(1.0 * confusion_matrix[cc, cc] / rec_denom[cc])
    return pr, rec


def add_base_args(parser: argparse.ArgumentParser):
    # Dataset arguments
    parser.add_argument('--target', default='wrgbd', choices=['wrgbd', 'ARID', 'ARID-RL75I', 'valHB'])
    parser.add_argument('--source', default='synARID', choices=['synARID', 'synHB'])
    parser.add_argument("--data_root_source", default=None)
    parser.add_argument("--data_root_target", default=None)
    parser.add_argument("--train_file_source", default=None)
    parser.add_argument("--test_file_source", default=None)
    parser.add_argument("--train_file_target", default=None)
    parser.add_argument("--test_file_target", default=None)
    parser.add_argument("--class_num", default=51)
    parser.add_argument('--depth_noise', default=None, choices=['n', 'ln'])

    parser.add_argument("--num_workers", default=4, type=int)
    parser.add_argument("--snapshot", default="snapshot/")
    parser.add_argument("--tensorboard", default="tensorboard/")
    parser.add_argument('--gpu', default=0)
    parser.add_argument('--suffix', default="")
    parser.add_argument('--test_batches', default=100, type=int)
    parser.add_argument('--resume', action='store_true')

    # hyper-params
    parser.add_argument("--net", default="resnet18")
    parser.add_argument("--epoch", default=20, type=int)
    parser.add_argument("--lr", default=0.0001, type=float)
    parser.add_argument("--lr_mult", default=1.0, type=float)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--weight_decay", default=0.05, type=float)
    parser.add_argument("--dropout_p", default=0.5)


def default_paths(args):
    print("{} -> {}".format(args.source, args.target))
    data_root_source, data_root_target, split_source_train, split_source_test, split_target = make_paths(
        source=args.source, target=args.target)
    args.data_root_source = args.data_root_source or data_root_source
    args.data_root_target = args.data_root_target or data_root_target
    args.train_file_source = args.train_file_source or split_source_train
    args.test_file_source = args.test_file_source or split_source_test
    args.train_file_target = args.train_file_target or split_target
    args.test_file_target = args.test_file_target or split_target


def make_paths(source='synARID', target='wrgbd'):
    """
    Supported targets:
        wrgbd (default):
            Washington RGB-D, remapped and with 4 classes removed
        ARID:
            Full ARID dataset (remapped)
        ARID-RL75I:
            ARID (remove less 75 info)
    """

    node = platform.node()

    data_root_source, data_root_target, split_source_train, split_source_test, split_target = None, None, None, None, \
                                                                                              None

    # Ares (lab)
    if node == 'ares':
        if source == 'synARID':
            data_root_source = '/scratch/dataset/synArid/synARID_crops_square'
            split_source_train = '/scratch/dataset/synArid/synARID_crops_square/synARID_50k-split_sync_train1.txt'
            split_source_test = '/scratch/dataset/synArid/synARID_crops_square/synARID_50k-split_sync_test1.txt'
        elif source == 'synHB':
            data_root_source = '/scratch/dataset/HB/HB_Syn_crops_square'
            split_source_train = '/scratch/dataset/HB/HB_Syn_crops_square/HB_Syn_crops_25k-split_sync_train1.txt'
            split_source_test = '/scratch/dataset/HB/HB_Syn_crops_square/HB_Syn_crops_25k-split_sync_test1.txt'

        if target == 'wrgbd':
            data_root_target = '/home/lucar/datasets/washington/dataset_mohammad'
            split_target = '/home/lucar/datasets/washington/splits_mohammad/wrgbd_40k-split_sync.txt'
        elif target == 'valHB':
            data_root_target = '/scratch/dataset/HB/HB_val_crops_square'
            split_target = '/scratch/dataset/HB/HB_val_crops_square/HB_val_crops_25k-split_sync.txt'
    # Hyperion (lab)
    elif node == 'hyperion':
        if source == 'synARID':
            data_root_source = '/data/Washington/synArid/synARID_crops_square'
            split_source_train = '/data/Washington/synArid/synARID_crops_square/synARID_50k-split_sync_train1.txt'
            split_source_test = '/data/Washington/synArid/synARID_crops_square/synARID_50k-split_sync_test1.txt'

        if target == 'wrgbd':
            data_root_target = '/data/Washington/dataset_mohammad'
            split_target = '/data/Washington/splits_mohammad/wrgbd_40k-split_sync.txt'
    # Athena (lab)
    elif node == 'athena-lab':
        if source == 'synARID':
            data_root_source = '/media/BackupB/data-lucar/synArid/synARID_crops_square'
            split_source_train = '/media/BackupB/data-lucar/synArid/synARID_crops_square/synARID_50k-split_sync_train1.txt'
            split_source_test = '/media/BackupB/data-lucar/synArid/synARID_crops_square/synARID_50k-split_sync_test1.txt'
        elif source == 'synHB':
            data_root_source = '/media/BackupB/data-lucar/HB/HB_Syn_crops_square'
            split_source_train = '/media/BackupB/data-lucar/HB/HB_Syn_crops_square/HB_Syn_crops_25k-split_sync_train1.txt'
            split_source_test = '/media/BackupB/data-lucar/HB/HB_Syn_crops_square/HB_Syn_crops_25k-split_sync_test1.txt'

        if target == 'wrgbd':
            data_root_target = '/media/BackupB/data-lucar/wrgbd'
            split_target = '/media/BackupB/data-lucar/wrgbd/wrgbd_40k-split_sync.txt'
        elif target == 'valHB':
            data_root_target = '/media/BackupB/data-lucar/HB/HB_val_crops_square'
            split_target = '/media/BackupB/data-lucar/HB/HB_val_crops_square/HB_val_crops_25k-split_sync.txt'

    # Cluster IIT Milano
    elif platform.node() == "frontend" or platform.node() == "node1" or platform.node() == "node2":
        if source == 'synARID':
            data_root_source = '/home/mohammad/my_datasets/synARID_crops_square'
            split_source_train = '/home/mohammad/my_datasets/synARID_crops_square/synARID_50k-split_sync_train1.txt'
            split_source_test = '/home/mohammad/my_datasets/synARID_crops_square/synARID_50k-split_sync_test1.txt'
        elif source == 'synHB':
            data_root_source = '/home/mohammad/my_datasets/HB_Syn_crops_square'
            split_source_train = '/home/mohammad/my_datasets/HB_Syn_crops_square/HB_Syn_crops_25k-split_sync_train1.txt'
            split_source_test = '/home/mohammad/my_datasets/HB_Syn_crops_square/HB_Syn_crops_25k-split_sync_test1.txt'

        if target == 'wrgbd':
            data_root_target = '/home/mohammad/my_datasets/wrgbd_eval_dataset'
            split_target = '/home/mohammad/my_datasets/wrgbd_eval_dataset/split_files_and_labels/wrgbd_40k-split_sync.txt'
        elif target == 'valHB':
            data_root_target = '/home/mohammad/my_datasets/HB_val_crops_square'
            split_target = '/home/mohammad/my_datasets/HB_val_crops_square/HB_val_crops_25k-split_sync.txt'

    if None in [data_root_source, data_root_target, split_source_train, split_source_test, split_target]:
        warnings.warn("Cannot set default paths for {} -> {} on this system ({})".format(source, target, node))

    return data_root_source, data_root_target, split_source_train, split_source_test, split_target


def map_to_device(device, t):
    return tuple(map(lambda x: x.to(device), t))
